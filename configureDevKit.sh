#!/bin/bash

set -e

# Подготовка примеров к работе. Обязательно надо запустить при изменении конфигурации ключей

rm -f ./OpenAPI/*.json
cp ./ExamplesBase/*.json ./OpenAPI


# для FileAPI надо подставить данные CreItemID  в шаблоны
CREITEMID=`cat ./config/creItemId.cfg`

echo "Read config from ./config:"
echo "creItemId.cfg is $CREITEMID"

cd ./ExamplesBase
for file in `find ./ -type f -name "*.json"`
do
echo "Convert file $file"
JSON=`CREITEMID=$CREITEMID  envsubst < $file`
echo $JSON | ../jq -r '.' > ../FileAPI/$file
done
cd ..

