#!/bin/bash

set -e

#
#	Шаг 2: Создание X-Api-Key для добавления документов в очередь созданной организации
#


JQ="../jq -r"
# Для работы скрипта необходимо предварительно задать значения:
#  1. ключа доступа по API для партнера -- X-Partner-Key  ./config/X-Partner-Key.cfg   ( выдаётся партнеру после заключения договоренности о сотрудничестве. Хранится партнером в тайне.)
#  2. ИД-организации для которой генерится ключ доступа к API X-Api-Key
XPARTNERKEY=`cat ../config/X-Partner-Key.cfg`
echo "Read config from ./config:"
echo "X-Partner-Key.cfg is $XPARTNERKEY"
ORGID=`cat organization.id`
echo "Organization id is $ORGID"



#	Теперь надо запросить для организации X-Api-Key
if [ -e ./lastIdempKeyXApiKey.id ]; then
IDEMPKEYXAPIKEY=`cat lastIdempKeyXApiKey.id`
else
IDEMPKEYXAPIKEY=$(cat /proc/sys/kernel/random/uuid)
echo "$IDEMPKEYXAPIKEY">lastIdempKeyXApiKey.id
fi
echo "IdempKeyXApiKey = $IDEMPKEYXAPIKEY"



JSON=`cat <<HEREDOC
{
  "idempKey": "$IDEMPKEYXAPIKEY", 
  "organizationId": "$ORGID",
  "title": "Основной ключ организации"
}
HEREDOC`


SERVER_ANSWER=`curl -v -H "Content-Type: application/json;charset=UTF-8" -H "X-Partner-Key:$XPARTNERKEY" -d "${JSON}" -X POST https://api.qkkmserver.ru/partner/api/v1/open-api-key`
echo "Get answer:"
echo $SERVER_ANSWER | $JQ '.'

# Из данного вывода нас интересует поле key -- сам ключ
ERRCODE=`echo $SERVER_ANSWER | $JQ '.code'`
XAPIKEY=''
if [ "$ERRCODE" -eq "0" ]; then
    XAPIKEY=`echo $SERVER_ANSWER | $JQ '.object.key'`
    echo "X-Api-Key created success. Key = $XAPIKEY"
else
    echo "Error create X-Api-Key for organization. Code is not 0: $ERRCODE"
fi

echo $XAPIKEY>X-Api-Key.cfg







