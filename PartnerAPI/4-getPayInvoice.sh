#!/bin/bash

set -e

#
#	Шаг 4: Запрашивается счет на оплату сервиса.
#       1. Генерация счета на сервера (создаётся запись со счетом)    2. Получение самого счета (счет выдаётся по хеш-ссылке. Время на сервере не тратится на создание счета. Он уже есть)

JQ="../jq -r"
XPARTNERKEY=`cat ../config/X-Partner-Key.cfg`
echo "Partner key is $XPARTNERKEY"

# для какой организации создаём счёт?
ORGID=`cat organization.id`
echo "Create invoice for organization $ORGID"


#	Генерация счета. Счет может генерировать только партнер.
SERVER_ANSWER=`curl -v -H "Content-Type: application/json;charset=UTF-8" -H "X-Partner-Key:$XPARTNERKEY" -X GET https://api.qkkmserver.ru/partner/api/v1/organizations/$ORGID/pay-invoice`
echo "Get answer:"
echo $SERVER_ANSWER | $JQ '.'

# Из данного вывода нас интересует поле key -- сам ключ для получения в последствии счета
ERRCODE=`echo $SERVER_ANSWER | $JQ '.code'`
echo "ErrCode=$ERRCODE"
KEY=''
if [ "$ERRCODE" -eq "0" ]; then
    KEY=`echo $SERVER_ANSWER | $JQ '.object.key'`
    echo "Create invoice success. Key for get invoice = $KEY"
else
    echo "Error create invoice. Code is not 0: $ERRCODE"
    exit $ERRCODE
fi

echo $KEY>invoice-key.id

# А теперь качаем счет в виде PDF
wget https://api.qkkmserver.ru/share/organizations/pay-invoice/$KEY
mv $KEY $KEY.pdf