#!/bin/bash

set -e

#
#	Шаг 1: создаём новую организацию
#


JQ="../jq -r"
# Для работы скрипта необходимо предварительно задать значения:
#  1. ключа доступа по API для партнера -- X-Partner-Key  ./config/X-Partner-Key.cfg   ( выдаётся партнеру после заключения договоренности о сотрудничестве. Хранится партнером в тайне.)

XPARTNERKEY=`cat ../config/X-Partner-Key.cfg`
echo "Read config from ./config:"
echo "X-Partner-Key.cfg is $XPARTNERKEY"


# генерирую ключ идемпотентности либо читаю из файла
if [ -e ./lastIdempKeyOrg.id ]; then
IDEMPKEYORG=`cat lastIdempKeyOrg.id`
else
IDEMPKEYORG=$(cat /proc/sys/kernel/random/uuid)
echo "$IDEMPKEYORG">lastIdempKeyOrg.id
fi
echo "IdempKeyOrg = $IDEMPKEYORG"


JSON=`cat <<HEREDOC
{
  "idempKey": "$IDEMPKEYORG",
  "inn": "1234567890",
  "name": "КЕМ_Тест_ПартнерАПИ",
  "email": "support-0@qkkmserver.ru",
  "phoneNumber": "79282701319",
  "chief": "РУКОВОДИТЕЛЬ ОРГАНИЗАЦИИ",
  "contact": "КОНТАКТНОЕ ЛИЦО",
  "card": {
    "legalAddress": "ЮРИДИЧЕСКИЙ АДРЕС",
    "postalAddress": "ПОЧТОВЫЙ АДРЕС",
    "physicalAddress": "ФАКТИЧЕСКИЙ АДРЕС",
    "ogrn": "",
    "transactionsAccount": "30101810600000000602",
    "corrAccount": "",
    "bik": "046015602",
    "bankTitle": "Юго-Западный банк Сбербанк России"
  }
}
HEREDOC`

echo "Prepare JSON request:"
echo $JSON | $JQ '.'


echo ""
echo "Send task to server....and read answer"
SERVER_ANSWER=`curl -v -H "Content-Type: application/json;charset=UTF-8" -H "X-Partner-Key:$XPARTNERKEY" -d "${JSON}" -X POST https://api.qkkmserver.ru/partner/api/v1/registration`
echo "Get answer:"
echo $SERVER_ANSWER | $JQ '.'


# Из данного вывода нас интересует ИД созданной организации
ERRCODE=`echo $SERVER_ANSWER | $JQ '.code'`
ORGID=''
if [ $ERRCODE="0" ]; then
    ORGID=`echo $SERVER_ANSWER | $JQ '.object.id'`
    echo "Organization created success. ID=$ORGID"
else
    echo "Error create organization. Code is not 0: $ERRCODE"
fi

echo $ORGID>organization.id

