#!/bin/bash

set -e

#
#	Шаг 3: запрашиваем РегКод для установки QKkmServer.NODE
#

JQ="../jq -r"
XAPIKEY=`cat X-Api-Key.cfg`
echo "Organization X-Api-Key is $XAPIKEY"


#	Получение ключа регистрации узла печати   RegKey
SERVER_ANSWER=`curl -v -H "Content-Type: application/json;charset=UTF-8" -H "X-Api-Key:$XAPIKEY" -d "" -X POST https://api.qkkmserver.ru/open/api/v1/nodes/reg-key`
echo "Get answer:"
echo $SERVER_ANSWER | $JQ '.'

# Из данного вывода нас интересует поле key -- сам ключ
ERRCODE=`echo $SERVER_ANSWER | $JQ '.code'`
echo "ErrCode=$ERRCODE"
REGKEY=''
if [ "$ERRCODE" -eq "0" ]; then
    REGKEY=`echo $SERVER_ANSWER | $JQ '.object.key'`
    echo "NODE RegKey created success. NODE RegKey = $REGKEY"
else
    echo "Error create NODE RegKey for organization. Code is not 0: $ERRCODE"
fi

echo $REGKEY>NODE-RegKey.id
