#!/bin/bash 

set -e

#
#	ПОЛУЧИТЬ СПИСОК ВСЕХ ККМ ЗАРЕГИСТРИРОВАННЫХ ЗА ОРГАНИЗАЦИЕЙ (ДОСТУПНЫХ В ЛК) И ИХ СОСТОЯНИЯ
#

# Для работы скрипта необходимо предварительно задать значения:
#  1. ключа доступа по API -- X-Api-Key  ./config/X-Api-Key.cfg   (создаётся в личном кабинете lk.qkkmserver.ru раздел Ключи API)
XAPIKEY=`cat ../config/X-Api-Key.cfg`
echo "Read config from ./config:"
echo "X-Api-Key.cfg is $XAPIKEY"

# отправляем документ на сервер
echo ""
echo "Send creItem list information request to server....and read answer"
SERVER_ANSWER=`curl -v -H "Content-Type: application/json;charset=UTF-8" -H "X-Api-Key:$XAPIKEY" -X GET https://api.qkkmserver.ru/open/api/v1/cre/items`
echo "Get answer:"
echo $SERVER_ANSWER | ../jq -r '.'