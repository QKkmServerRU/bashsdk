#!/bin/bash 

set -e

#
#	ПОЛУЧИТЬ ИНФОРМАЦИЮ О ТЕКУЩЕМ СОСТОЯНИИ КОНКРЕТНОЙ ККМ ПО ЕЁ ИД (CREITEMID)
#


# Для работы скрипта необходимо предварительно задать значения:
#  1. ключа доступа по API -- X-Api-Key  ./config/X-Api-Key.cfg   (создаётся в личном кабинете lk.qkkmserver.ru раздел Ключи API)
#  2. идентификатора ККМ   -- CreItemId  ./config/creItemId.cfg   (см. в личном кабинете lk.qkkmserver.ru раздел ККМ, Настройки, ID)
XAPIKEY=`cat ../config/X-Api-Key.cfg`
CREITEMID=`cat ../config/creItemId.cfg`
echo "Read config from ./config:"
echo "X-Api-Key.cfg is $XAPIKEY"
echo "creItemId.cfg is $CREITEMID"

# отправляем документ на сервер
echo ""
echo "Send creItem information request to server....and read answer"
SERVER_ANSWER=`curl -v -H "Content-Type: application/json;charset=UTF-8" -H "X-Api-Key:$XAPIKEY" -X GET https://api.qkkmserver.ru/open/api/v1/cre/items/$CREITEMID`
echo "Get answer:"
echo $SERVER_ANSWER | ../jq -r '.'