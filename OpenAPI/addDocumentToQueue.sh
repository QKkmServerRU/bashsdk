#!/bin/bash 

set -e

#
#	ДОБАВЛЕНИЕ В ОБЛАЧНУЮ ОЧЕРЕДЬ ДОКУМЕНТА И КОНТРОЛЬ ЕГО ИСПОЛНЕНИЯ
#


JQ="../jq -r"
HOST="qapi.itkassa.ru"
# Для работы скрипта необходимо предварительно задать значения:
#  1. ключа доступа по API -- X-Api-Key  ./config/X-Api-Key.cfg   (создаётся в личном кабинете lk.qkkmserver.ru раздел Ключи API)
#  2. идентификатора ККМ   -- CreItemId  ./config/creItemId.cfg   (см. в личном кабинете lk.qkkmserver.ru раздел ККМ, Настройки, ID)
XAPIKEY=`cat ../config/X-Api-Key.cfg`
CREITEMID=`cat ../config/creItemId.cfg`
echo "Read config from ./config:"
echo "X-Api-Key.cfg is $XAPIKEY"
echo "creItemId.cfg is $CREITEMID"

# На вход скрипту передаётся имя файла-задания
FILENAME=$1
echo "Run task from file $FILENAME"

# В переданном файле необходимо произвести замену $CREITEMID на значение идентификатора ККМ
JSON=`CREITEMID=$CREITEMID  envsubst < $FILENAME`
echo "Prepared task for send to server:"
echo $JSON | $JQ '.'

# отправляем документ на сервер
echo ""
echo "Send task to server....and read answer"
SERVER_ANSWER=`curl -v -H "Content-Type: application/json;charset=UTF-8" -H "X-Api-Key:$XAPIKEY" -d "${JSON}" -X POST https://${HOST}/open/api/v1/print-documents`
echo "Get answer:"
echo $SERVER_ANSWER | $JQ '.'

# Документ попадает в очередь. Получаю идентификатор задания в очереди
TASKID=`echo $SERVER_ANSWER | $JQ '.object.id'`
echo "Extracted taskId from answer is $TASKID"

# Проверим ход исполнения задания. Для этого запустим цикл опроса состояния задания в очереди.
echo "Start monitoring task state from queue"
RESULT_STATE="null"
while true
do
    CURRENT_STATE=`curl -v -H "Content-Type: application/json;charset=UTF-8" -H "X-Api-Key:$XAPIKEY" -X GET https://${HOST}/open/api/v1/print-documents/$TASKID`
    echo $CURRENT_STATE | $JQ '.'
    RESULT_STATE=`echo $CURRENT_STATE | $JQ '.object.resultItem'`
    echo $RESULT_STATE  | $JQ '.'
    if [[ $RESULT_STATE == 'null' ]]; then
	# определяю статус задания в очереди
	STATUS_IN_QUEUE=`echo $CURRENT_STATE | $JQ '.object.queueItem.status'`
	echo "Current state IN QUEUE (object -> resultItem == null) is $STATUS_IN_QUEUE"
	sleep 1s
    else
	echo "Current state DONE"
	break
    fi
done

# Теперь осталось определить с каким результатом закончилось исполнение задания
# Для этого смотрим сначала на глобальный результат печати. В случае, когда всё хорошо он имеет значение success
S_RESULT=`echo $RESULT_STATE | $JQ '.result'`
S_CREERRORID=`echo $RESULT_STATE | $JQ '.creErrorId'`
S_CREERRORTEXT=`echo $RESULT_STATE | $JQ '.creErrorText'`
if [[ $R_RESULT == 'success' ]]; then
    # успешное исполнение команды
    echo "Command done $S_RESULT. ErrorCode=$S_CREERRORID and message=$S_CREERRORTEXT"
else
    # произошла какая-то ошибка
    echo "Command done $S_RESULT. ErrorCode=$S_CREERRORID and message=$S_CREERRORTEXT"
fi