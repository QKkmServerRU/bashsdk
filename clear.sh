#!/bin/bash

set -e


#	Очистка параметров / переменных

echo "">./config/X-Api-Key.cfg
echo "">./config/X-Partner-Key.cfg
echo "">./config/creItemId.cfg

rm -f ./PartnerAPI/*.id
rm -f ./PartnerAPI/*.cfg

rm -f ./OpenAPI/*.json

rm -f ./FileAPI/*.json